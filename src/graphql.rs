//! Provides types and methods to interact with Connectome GraphQL API

use std::sync::Arc;

use anyhow::{Context, Result};
use graphql_client::{GraphQLQuery, QueryBody, Response};
use http::Uri;
use reqwest::Client;
use serde::Serialize;
use serde_json::Value;
use tokio::sync::Mutex;
use tracing::warn;

use crate::{
    authn::{self, AccessToken},
    config::Config,
};

use self::fetch::ResponseData;

/// Request for a single Dataset
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schemas/connectome_schema.graphql",
    query_path = "schemas/fetch_query.graphql",
    response_derives = "Debug, Serialize",
    skip_serializing_none
)]
pub struct Fetch;

/// GraphQL query error
#[derive(Serialize)]
pub struct ResultError {
    /// Error message
    pub message: String,
    /// Location in file
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locations: Option<Vec<Location>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<Vec<String>>,
}

impl From<graphql_client::Error> for ResultError {
    fn from(value: graphql_client::Error) -> Self {
        let message = value.message;
        let locations = value.locations.map(|vec| {
            vec.into_iter()
                .map(|loc| Location {
                    line: loc.line,
                    column: loc.column,
                })
                .collect::<Vec<Location>>()
        });
        let path = value.path.map(|vec| {
            vec.into_iter()
                .map(|pf| pf.to_string())
                .collect::<Vec<String>>()
        });
        Self {
            message,
            locations,
            path,
        }
    }
}

#[derive(Serialize)]
pub struct Location {
    pub line: i32,
    pub column: i32,
}

#[derive(Default)]
pub struct FetchResult {
    pub data: Option<ResponseData>,
    pub errors: Option<Vec<FetchError>>,
}

pub struct FetchError {
    #[allow(dead_code)]
    pub title: Option<String>,
    pub message: Option<String>,
    pub status_code: Option<u64>,
}

impl FetchError {
    pub fn not_found(&self) -> bool {
        self.status_code.is_some_and(|sc| sc == 400)
    }
    pub fn not_authenticated(&self) -> bool {
        self.status_code.is_some_and(|sc| sc == 403)
    }
}

/// Gets a single record from GraphQL endpoint
pub async fn get_record(
    id: &str,
    config: Arc<Config>,
    client: Arc<Client>,
    access_token: Arc<Mutex<AccessToken>>,
) -> Result<FetchResult> {
    let mut access_token = access_token.lock().await;
    let mut attempted_reauth = false;
    loop {
        let iri = format!("https://data.connectome.ch/memobase/dataset/{id}");
        let variables = fetch::Variables { iri };
        let graphql_res = fetch_query(
            client.clone(),
            &config.graphql_endpoint,
            &access_token.token_value,
            variables,
        )
        .await
        .with_context(|| format!("Getting datasets from {} failed", &config.graphql_endpoint))?;
        let errors = retrieve_errors(graphql_res.errors.as_deref());
        // With Switch's new authorisation measures, this shouldn't be strictly necessary
        // anymore, as the fetch endpoint won't return a not authenticated error anymore.
        // Nevertheless, we leave this code in place at least for the moment.
        if let Some(ref err) = errors {
            if err.iter().any(FetchError::not_authenticated) && !attempted_reauth {
                warn!("Client not authenticated! Getting new access token");
                *access_token = authn::get_access_token(
                    &client,
                    &config.token_endpoint,
                    &config.client_id,
                    &config.client_secret,
                )
                .await
                .with_context(|| {
                    format!(
                        "Getting access token from {} failed",
                        &config.token_endpoint
                    )
                })?;
                attempted_reauth = true;
                continue;
            }
        };
        return Ok(FetchResult {
            data: graphql_res.data,
            errors,
        });
    }
}

/// Build and set fetch query to endpoint
pub async fn fetch_query(
    client: Arc<Client>,
    graphql_endpoint: &Uri,
    access_token: &str,
    variables: fetch::Variables,
) -> Result<graphql_client::Response<fetch::ResponseData>> {
    let query_body: QueryBody<fetch::Variables> = Fetch::build_query(variables);
    let res = client
        .post(graphql_endpoint.to_string())
        .header("Authorization", format!("Bearer {access_token}"))
        .json(&query_body)
        .send()
        .await
        .context("Request to Connectome GraphQL API failed")?;
    let res_body: Response<fetch::ResponseData> = res
        .json()
        .await
        .context("Parsing response body as JSON failed")?;
    Ok(res_body)
}

/// Retrieves most important information from error message
pub fn retrieve_errors(err: Option<&[graphql_client::Error]>) -> Option<Vec<FetchError>> {
    err.map(|err| {
        err.iter()
            .filter_map(|e| e.extensions.as_ref())
            .filter_map(|ext| ext.get("originalError"))
            .filter_map(Value::as_object)
            .map(|oe| FetchError {
                title: oe
                    .get("error")
                    .and_then(Value::as_str)
                    .map(ToOwned::to_owned),
                message: oe
                    .get("message")
                    .and_then(Value::as_str)
                    .map(ToOwned::to_owned),
                status_code: oe.get("statusCode").and_then(Value::as_u64),
            })
            .collect()
    })
}
