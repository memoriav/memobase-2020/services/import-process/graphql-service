//! Intermediary between the the [Connectome](https://opendatanavigator.switch.ch/) GraphQL API and
//! the [Normalization Service](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/normalization-service)
//!
//! Its responsibility is to fetch linked and potentially enriched Named Entities (NEs) from
//! Connectome on request. These NEs are extracted beforehand by Connectome from descriptive texts
//! found in Memobase documents.

use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
    process,
    sync::Arc,
    time::Duration,
};

use anyhow::Result;
use anyhow::{bail, Context};
use api::models::AppState;
use cache::{Cache, NelloContext};
use flate2::read::GzDecoder;
use reqwest::Client;

use tokio::sync::Mutex;
use tracing::{debug, error, info};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod api;
mod authn;
mod cache;
mod config;
mod graphql;
mod internal_model;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "graphql_service=debug".into())
                .add_directive("h2=warn".parse()?)
                .add_directive("hyper=warn".parse()?)
                .add_directive("reqwest=warn".parse()?)
                .add_directive("rustls=warn".parse()?)
                .add_directive("sled=warn".parse()?),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    debug!("Reading configuration");
    let config = config::read_settings().context("Reading configuration failed")?;

    debug!("Starting up cache");
    let cache = Arc::new(Mutex::new(
        cache::Cache::open("db/").context("Opening database failed")?,
    ));
    info!(
        "Reading NELLO context file from {}",
        &config.nello_ctx_file_path
    );
    let contexts_num = load_nello_ctx(&config.nello_ctx_file_path, cache.clone()).await?;
    debug!(
        "NELLO context file {} read: {} records saved in DB",
        &config.nello_ctx_file_path, contexts_num
    );
    debug!("Building HTTP client");
    let client = Client::new();
    debug!("Getting access token");
    let access_token = authn::get_access_token(
        &client,
        &config.token_endpoint,
        &config.client_id,
        &config.client_secret,
    )
    .await
    .with_context(|| {
        format!(
            "Getting access token from {} failed",
            &config.token_endpoint
        )
    })?;

    let cloned_cache = cache.clone();

    spawn_cache_purging(cloned_cache, config.purge_interval);

    info!("Starting webserver on {}", config.host);

    let mtls_config = Arc::new(Mutex::new(
        tokio_mtls_utils::mtls::Config::new(
            &config.server_key_path,
            &config.server_cert_path,
            config.ca_cert_path.as_deref(),
            &config
                .allowed_client_domains
                .iter()
                .map(String::as_str)
                .collect::<Vec<&str>>(),
        )
        .unwrap(),
    ));

    let cert_reload_task = tokio_mtls_utils::renew_certificates(
        mtls_config.clone(),
        Duration::from_secs(config.cert_reload_interval),
    )
    .await
    .unwrap();

    let host = config.host.clone();
    let token_refresh_inverval = config.token_refresh_interval;

    let app_state = Arc::new(Mutex::new(api::models::AppState {
        config: Arc::new(config),
        cache,
        client: Arc::new(client),
        access_token: Arc::new(Mutex::new(access_token)),
    }));

    let shared_app_state = app_state.clone();

    spawn_access_token_refresh(shared_app_state, token_refresh_inverval);

    let routes = api::routes().with_state(app_state);

    let webserver_task =
        tokio::spawn(async move { tokio_mtls_utils::serve(routes, mtls_config, host).await });
    match tokio::try_join!(cert_reload_task, webserver_task) {
        Ok(_) => {
            info!("All done. Shutting down application");
            Ok(())
        }
        Err(e) => bail!("An error happened: {e}. Shutting down application"),
    }
}

async fn load_nello_ctx(file_path: &str, cache: Arc<Mutex<Cache>>) -> Result<usize> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let decoder = GzDecoder::new(reader);
    let mut line_reader = BufReader::new(decoder);
    let mut cache = cache.lock().await;
    let mut num_bytes;
    let mut buffer = String::new();
    let mut i = 0;
    loop {
        num_bytes = line_reader.read_line(&mut buffer)?;
        if num_bytes == 0 {
            break;
        }
        let nello_context: Result<HashMap<String, Vec<NelloContext>>, _> =
            serde_json::from_str(&buffer);
        let nello_context = match nello_context {
            Ok(ctx) => ctx,
            Err(e) => {
                dbg!(&buffer);
                error!("{}", e);
                continue;
            }
        };
        if let Some((k, v)) = nello_context.iter().next() {
            cache.set_nello_ctx(k, v)?;
        }
        buffer.clear();
        i += 1;
    }
    Ok(i)
}

fn spawn_cache_purging(cache: Arc<Mutex<Cache>>, purge_interval: u64) {
    tokio::spawn(async move {
        let mut purge_retries = 0;
        loop {
            tokio::time::sleep(Duration::from_secs(purge_interval)).await;
            if let Ok(i) = cache.lock().await.purge() {
                purge_retries = 0;
                info!("{} documents purged from cache", i);
            } else {
                purge_retries += 1;
            }
            if purge_retries > 3 {
                error!("Too many retries while purging the cache");
                process::exit(1);
            }
        }
    });
}

fn spawn_access_token_refresh(app_state: Arc<Mutex<AppState>>, refresh_interval: u64) {
    tokio::spawn(async move {
        loop {
            tokio::time::sleep(Duration::from_secs(refresh_interval)).await;
            app_state.lock().await.refresh_access_token().await;
            info!(
                "Access token refreshed. Next refresh due in {} seconds.",
                refresh_interval
            );
        }
    });
}
