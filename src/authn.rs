//! Provides the means to request and handle Connectome access tokens

use std::collections::HashMap;

use anyhow::{bail, Context, Result};
use http::Uri;
use reqwest::Client;
use serde::{Deserialize, Deserializer};

/// Access token returned by respective endpoint
#[derive(Debug, Deserialize)]
pub struct AccessToken {
    /// The actual access token. Send this value along with requests to the GraphQL API
    #[serde(rename = "access_token")]
    pub token_value: String,
    /// Scope of access token
    #[allow(dead_code)]
    #[serde(deserialize_with = "string_tokenizer")]
    pub scope: Vec<String>,
}

/// Returns vector of strings by deserializing and splitting string at whitespaces
fn string_tokenizer<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    Ok(s.split(' ').map(ToString::to_string).collect())
}

/// Gets access token from respective endpoint
pub async fn get_access_token(
    client: &Client,
    endpoint_uri: &Uri,
    client_id: &str,
    client_secret: &str,
) -> Result<AccessToken> {
    let mut form_body = HashMap::new();
    form_body.insert("grant_type", "client_credentials");
    form_body.insert("client_id", client_id);
    form_body.insert("client_secret", client_secret);
    let response = client
        .post(endpoint_uri.to_string())
        //.header("Content-Type", "application/x-www-form-urlencoded")
        .form(&form_body)
        .send()
        .await?;
    if !response.status().is_success() {
        bail!(
            "Getting access token failed. HTTP code {} returned by server",
            response.status().as_u16()
        );
    }
    let json: AccessToken = response
        .json()
        .await
        .context("Parsing response body as access token failed")?;
    Ok(json)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_access_token_parsing() {
        let access_token = r#"{"access_token":"abcd","expires_in":86400,"refresh_expires_in":0,"token_type":"Bearer","not-before-policy":0,"scope":"email odn-b2b silver profile"}"#;
        let res: Result<AccessToken, serde_json::Error> = serde_json::from_str(access_token);
        assert!(res.is_ok());
        assert!(res.unwrap().scope.contains(&"silver".to_string()));
    }
}
