#![allow(clippy::doc_markdown)]
//! Endpoints offered by the RESTful API
//!
//! This module provides the endpoints which are exposed by the server's RestAPI

use std::sync::Arc;

use anyhow::anyhow;
use axum::{
    extract::{Path, State},
    response::IntoResponse,
    routing::get,
    Json, Router,
};
use http::StatusCode;
use tokio::sync::Mutex;
use tracing::{debug, error, warn};

use crate::{
    api::models::InternalError,
    graphql::{self, FetchError},
    internal_model::{self, EnrichedRecord},
};

use self::models::AppError;

pub mod models {
    //! Models used by the API

    use std::{error::Error, sync::Arc};

    use reqwest::Client;
    use tokio::sync::Mutex;

    use crate::{
        authn::{get_access_token, AccessToken},
        cache::Cache,
        config::Config,
    };

    /// State needed by the API handlers
    #[derive(Clone, Debug)]
    pub struct AppState {
        pub config: Arc<Config>,
        pub cache: Arc<Mutex<Cache>>,
        pub client: Arc<Client>,
        pub access_token: Arc<Mutex<AccessToken>>,
    }

    impl AppState {
        pub async fn refresh_access_token(&mut self) {
            let access_token = get_access_token(
                &self.client,
                &self.config.token_endpoint,
                &self.config.client_id,
                &self.config.client_secret,
            )
            .await
            // TODO: Remove unwrap
            .unwrap();
            self.access_token = Arc::new(Mutex::new(access_token));
        }
    }

    /// Possible HTTP errors
    pub(super) enum AppError {
        NotFound(String),
        InternalError(InternalError),
    }

    pub(super) struct InternalError(pub Box<dyn Error>);
}

impl IntoResponse for AppError {
    fn into_response(self) -> axum::response::Response {
        match self {
            Self::InternalError(e) => (StatusCode::INTERNAL_SERVER_ERROR, e.0.to_string()),
            Self::NotFound(id) => (StatusCode::NOT_FOUND, format!("{id} not found")),
        }
        .into_response()
    }
}

impl From<anyhow::Error> for models::InternalError {
    fn from(value: anyhow::Error) -> Self {
        Self(value.into())
    }
}

pub fn routes() -> Router<Arc<Mutex<models::AppState>>> {
    Router::new()
        .route("/health", get(|| async { "OK" }))
        .route("/record/{record_id}", get(get_record))
}

async fn get_record(
    Path(record_id): Path<String>,
    State(state): State<Arc<Mutex<models::AppState>>>,
) -> Result<Json<EnrichedRecord>, models::AppError> {
    debug!("Receiving request for {record_id}");
    let state = state.lock().await;
    let mut cache = state.cache.lock().await;
    if let Ok(Some(cached_value)) = cache.get(&record_id) {
        debug!("Record {record_id} already cached");
        Ok(Json(cached_value))
    } else {
        match graphql::get_record(
            &record_id,
            state.config.clone(),
            state.client.clone(),
            state.access_token.clone(),
        )
        .await
        {
            Ok(ref r)
                if r.errors
                    .as_ref()
                    .is_some_and(|e| e.iter().any(FetchError::not_found)) =>
            {
                debug!("Record {record_id} not found on Connectome");
                Err(AppError::NotFound(format!("Record {record_id}")))
            }
            Ok(ref r) if r.errors.is_some() => {
                let errors = r
                    .errors
                    .as_ref()
                    .unwrap()
                    .iter()
                    .fold(String::new(), |mut acc, e| {
                        if let Some(m) = e.message.as_ref() {
                            acc.push_str(m);
                        }
                        acc
                    });
                debug!(
                    "Request for record {record_id} in Connectome returned errors: {}",
                    &errors
                );
                Err(AppError::InternalError(InternalError::from(anyhow!(
                    "Data with errors: {errors}",
                ))))
            }
            Ok(r) if r.data.is_some() => {
                let nello_ctx = cache.get_nello_ctx(&format!(
                    "https://data.connectome.ch/memobase/dataset/{}",
                    &record_id
                ));
                let nello_ctx = match nello_ctx {
                    Ok(Some(ctx)) => ctx,
                    Ok(None) => {
                        let msg =
                            format!("Nello context for record with id {} not found", &record_id);
                        warn!("{}", &msg);
                        return Err(AppError::NotFound(msg));
                    }
                    Err(e) => {
                        let msg = format!(
                            "Fetching Nello context for record with id {} failed: {}",
                            &record_id, e
                        );
                        error!("{}", &msg);
                        return Err(AppError::InternalError(InternalError::from(anyhow!(e))));
                    }
                };
                let enriched_record = match internal_model::transform(r, &record_id, &nello_ctx) {
                    Ok(im) => im,
                    Err(e) => return Err(AppError::InternalError(InternalError::from(e))),
                };
                let caching_res =
                    cache.set(&record_id, &enriched_record, state.config.retention_time);
                if let Err(e) = caching_res {
                    error!("Caching of record with id {} failed: {}", record_id, e);
                }
                debug!(
                    "Named entities for record {record_id} successfully requested from Connectome"
                );
                Ok(Json(enriched_record))
            }
            Ok(_) => {
                warn!("No data in record {record_id}");
                Err(AppError::NotFound(format!(
                    "No data in record with id {record_id}"
                )))
            }
            Err(e) => {
                warn!(
                    "Internal server error received when requesting record {record_id} from Connectome: {e}"
                );
                Err(AppError::InternalError(InternalError::from(e)))
            }
        }
    }
}
