//! Internal Memobase model for enriched data

use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};

use crate::{
    cache::NelloContext,
    graphql::{fetch::FetchFetch, FetchResult},
};

/// A Memobase record with named entities linked to and enriched by Wikidata
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EnrichedRecord {
    /// Memobase ID
    pub id: String,
    /// List of recognised spatial entities
    pub spatials: Vec<Entity>,
    /// List of recognised agent entities
    pub agents: Vec<Entity>,
}

/// A named entity linked with a Wikidata entry
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Entity {
    /// List of referenced resources
    pub same_as: Vec<String>,
    /// Original text of entity
    pub text: String,
    /// Start offset of recognised entity
    pub start: usize,
    /// End offset of recognised entity
    pub end: usize,
}

impl Entity {
    fn build_from_cache_entry(same_as: Vec<String>, nello_ctx: &[NelloContext]) -> Option<Entity> {
        if let Some(wd_id) = same_as.first() {
            if let Some(ctx) = nello_ctx
                .iter()
                .find(|elem| Some(elem.kbid.as_ref()) == wd_id.split('/').last())
            {
                return Some(Entity {
                    same_as,
                    text: ctx.text.clone(),
                    start: ctx.start,
                    end: ctx.end,
                });
            }
        }
        None
    }
}

pub fn transform(res: FetchResult, id: &str, nello_ctx: &[NelloContext]) -> Result<EnrichedRecord> {
    if let Some(data) = res.data {
        if let FetchFetch::Dataset(ds) = data.fetch {
            Ok(EnrichedRecord {
                id: id.to_string(),
                spatials: if let Some(spatial) = ds.spatial_coverage {
                    spatial
                        .into_iter()
                        .filter_map(|elem| {
                            let same_as = elem.same_as.unwrap_or_default();
                            Entity::build_from_cache_entry(same_as, nello_ctx)
                        })
                        .collect()
                } else {
                    vec![]
                },
                agents: if let Some(about) = ds.about {
                    about
                        .into_iter()
                        .filter_map(|elem| {
                            let same_as = match elem {
                                crate::graphql::fetch::FetchFetchOnDatasetAbout::Organization(
                                    o,
                                ) => o.same_as.unwrap_or(vec![]),
                                crate::graphql::fetch::FetchFetchOnDatasetAbout::Person(p) => {
                                    p.same_as.unwrap_or(vec![])
                                }
                                _ => vec![],
                            };
                            Entity::build_from_cache_entry(same_as, nello_ctx)
                        })
                        .collect()
                } else {
                    vec![]
                },
            })
            /*Ok(ds
            .spatial_coverage
            .map(|spatial| {
                spatial
                    .into_iter()
                    .flat_map(|elem| elem.same_as.map(|sa| sa.into_iter().map(|sa| sa)))
            })
            .collect::<Vec<String>>())*/
        } else {
            Err(anyhow!("No dataset available"))
        }
    } else {
        Err(anyhow!("No data in GraphQL API response available!"))
    }
}
