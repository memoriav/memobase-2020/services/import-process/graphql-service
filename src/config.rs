//! Provides the utils to read and merge the application settings

use std::{
    fs::File,
    io::{BufReader, Read},
};

use anyhow::Context;
use clap::Parser;
use http::Uri;
use serde::Deserialize;
use tracing::info;

#[derive(Deserialize, Default, Parser)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
#[command(author, version, about, long_about = None)]
#[clap(rename_all = "kebab-case")]
struct InputConfig {
    /// Host address
    #[serde(default)]
    #[arg(short = 'H', long, env, verbatim_doc_comment)]
    pub host: Option<String>,
    /// Path to server certificate file
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub server_cert_path: Option<String>,
    /// Path to server key file
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub server_key_path: Option<String>,
    /// Path to CA certificate file
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub ca_cert_path: Option<String>,
    /// Interval in secs for reloading certificates
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub cert_reload_interval: Option<u64>,
    /// List of allowed client domains
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment, value_delimiter = ',')]
    pub allowed_client_domains: Option<Vec<String>>,
    /// GraphQL endpoint URI
    #[serde(default)]
    #[arg(short, long, env, verbatim_doc_comment)]
    pub graphql_endpoint: Option<String>,
    /// Access token endpoint URI
    #[serde(default)]
    #[arg(short, long, env, verbatim_doc_comment)]
    pub token_endpoint: Option<String>,
    /// Access token refresh interval in secs
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub token_refresh_interval: Option<u64>,
    /// Client id (required for getting access token)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub client_id: Option<String>,
    /// Client secret (required for getting access token)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub client_secret: Option<String>,
    /// Retention time of records in cache
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub retention_time: Option<i64>,
    /// Interval in secs at which the cache is emptied
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub purge_interval: Option<u64>,
    /// Path to NEL-LO context file
    /// File contains context information on each record enriched with NEL-LO
    /// Content must be formatted as JSON lines and file must be gzipped
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub nello_ctx_file_path: Option<String>,
    /// Path to configuration file
    #[serde(skip_deserializing)]
    #[arg(short = 'c', long, env)]
    pub config_file: Option<String>,
}

#[derive(Debug)]
pub struct Config {
    /// Host address
    pub host: String,
    /// Path to server certificate file
    pub server_cert_path: String,
    /// Path to server key file
    pub server_key_path: String,
    /// Path to CA certificate file
    pub ca_cert_path: Option<String>,
    /// Interval in secs for reloading certificates
    pub cert_reload_interval: u64,
    /// List of allowed client domains
    pub allowed_client_domains: Vec<String>,
    /// GraphQL endpoint URI
    pub graphql_endpoint: Uri,
    /// Access token endpoint URI
    pub token_endpoint: Uri,
    /// Access token refresh interval in secs
    pub token_refresh_interval: u64,
    /// Client id (required for getting access token)
    pub client_id: String,
    /// Client secret (required for getting access token)
    pub client_secret: String,
    /// Retention time of records in cache
    pub retention_time: i64,
    /// Interval in secs at which the cache is emptied
    pub purge_interval: u64,
    /// Path to NEL-LO context file
    pub nello_ctx_file_path: String,
}

impl Config {
    pub(self) fn build(cli_config: InputConfig, file_config: InputConfig) -> anyhow::Result<Self> {
        Ok(Self {
            host: cli_config
                .host
                .or(file_config.host)
                .unwrap_or("0.0.0.0:3000".to_string()),
            server_cert_path: cli_config
                .server_cert_path
                .or(file_config.server_cert_path)
                .context("setting cert_path required")?,
            server_key_path: cli_config
                .server_key_path
                .or(file_config.server_key_path)
                .context("setting cert_path required")?,
            ca_cert_path: cli_config.ca_cert_path.or(file_config.ca_cert_path),
            cert_reload_interval: cli_config
                .cert_reload_interval
                .or(file_config.cert_reload_interval)
                .unwrap_or(30 * 60),
            allowed_client_domains: cli_config
                .allowed_client_domains
                .or(file_config.allowed_client_domains)
                .context("list of allowed client domains required")?,
            graphql_endpoint: cli_config
                .graphql_endpoint
                .or(file_config.graphql_endpoint)
                .and_then(|ge| ge.parse::<Uri>().ok())
                .context("setting graphql_endpoint required")?,
            token_endpoint: cli_config
                .token_endpoint
                .or(file_config.token_endpoint)
                .and_then(|te| te.parse::<Uri>().ok())
                .context("setting token_endpoint required")?,
            token_refresh_interval: cli_config
                .token_refresh_interval
                .or(file_config.token_refresh_interval)
                .unwrap_or(2 * 60 * 60),
            client_id: cli_config
                .client_id
                .or(file_config.client_id)
                .context("setting client_id required")?,
            client_secret: cli_config
                .client_secret
                .or(file_config.client_secret)
                .context("setting client_id required")?,
            retention_time: cli_config
                .retention_time
                .or(file_config.retention_time)
                .unwrap_or(7 * 24 * 60 * 60),
            purge_interval: cli_config
                .purge_interval
                .or(file_config.purge_interval)
                .unwrap_or(60 * 60),
            nello_ctx_file_path: cli_config
                .nello_ctx_file_path
                .or(file_config.nello_ctx_file_path)
                .unwrap_or("nello_context.jsonl.gz".to_string()),
        })
    }
}

/// Creates a [`Config`] instance from the different possible input sources
pub fn read_settings() -> anyhow::Result<Config> {
    let cli_config = InputConfig::parse();
    let path = cli_config
        .config_file
        .clone()
        .unwrap_or("configs/main.toml".to_string());
    let file_config = if let Ok(file) = File::open(&path) {
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf)?;
        toml::from_str(&buf)?
    } else {
        info!("No config file found under {}", &path);
        InputConfig::default()
    };
    Config::build(cli_config, file_config)
}
