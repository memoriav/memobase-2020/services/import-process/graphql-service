//! Provides the [`Cache`] struct to temporarily store retrieved documents

use std::{
    io,
    ops::{Deref, DerefMut},
    thread,
};

use anyhow::{anyhow, bail, Context, Result};
use chrono::{Duration, Utc};
use serde::{Deserialize, Serialize};
use sled::IVec;
use tokio::time;
use tracing::{debug, error};

use crate::internal_model::EnrichedRecord;

const RECORD_NS: &str = "records";
const NELLO_NS: &str = "nello";

/// A cached value with an expiration time
#[derive(Clone, Debug, Deserialize, Serialize)]
struct CachedValue {
    record: EnrichedRecord,
    valid_through: i64,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct NelloContext {
    pub text: String,
    pub start: usize,
    pub end: usize,
    pub kbid: String,
}

/// Wrapper around a [`sled::Db`] instance providing getter, setter and maintenance methods for
/// storing documents
#[derive(Debug)]
pub struct Cache(sled::Db);

impl Deref for Cache {
    type Target = sled::Db;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Cache {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Cache {
    /// Opens a new [`Cache`] instance
    pub fn open(path: &str) -> Result<Self> {
        let mut retries = 0;
        loop {
            match sled::open(path) {
                Ok(db) => return Ok(Self(db)),
                Err(e) => {
                    if let sled::Error::Io(ref io_e) = e {
                        if io_e.kind() == io::ErrorKind::WouldBlock {
                            if retries > 5 {
                                bail!("Opening sled DB failed: Too many retries to unlock DB");
                            }
                            retries += 1;
                            thread::sleep(time::Duration::from_secs(10));
                            continue;
                        }
                    }
                    bail!("Opening sled DB failed: {}", e);
                }
            }
        }
    }

    /// Gets a document from the cache
    pub fn get(&self, record_id: &str) -> Result<Option<EnrichedRecord>> {
        let records = self
            .open_tree(RECORD_NS)
            .context("Opening DB namespace `records` failed")?;
        let uid = cityhasher::hash::<u32>(record_id).to_ne_bytes();
        records
            .get(uid)
            .map(|m| {
                m.and_then(|cv| {
                    let cached_value: Option<CachedValue> = serde_json::from_slice(&cv).ok();
                    cached_value
                })
                .filter(|cv| cv.valid_through > Utc::now().timestamp())
                .map(|cv| cv.record)
            })
            .map_err(Into::into)
    }

    /// Purges all expired documents and returns the sum of these documents
    pub fn purge(&mut self) -> Result<usize> {
        let records = self
            .open_tree(RECORD_NS)
            .context("Opening DB namespace `records`failed")?;
        let now = Utc::now().timestamp();
        let mut i = 0;
        records.iter().flatten().for_each(|(uid, rec)| {
            if let Ok(CachedValue {
                valid_through,
                record: _,
            }) = serde_json::from_slice(&rec)
            {
                if valid_through < now {
                    if let Err(e) = records.remove(&uid) {
                        error!(
                            "Couldn't remove record with uid {}: {}",
                            ivec_as_u32(&uid)
                                .map(|id| format!("with uid {id}"))
                                .unwrap_or_default(),
                            e
                        );
                    } else {
                        i += 1;
                        debug!(
                            "Record {} purged",
                            ivec_as_u32(&uid)
                                .map(|id| format!("with uid {id}"))
                                .unwrap_or_default(),
                        );
                    }
                }
            };
        });
        Ok(i)
    }

    /// Stores a new document in the cache
    pub fn set(
        &mut self,
        record_id: &str,
        record: &EnrichedRecord,
        retention_period: i64,
    ) -> Result<()> {
        let records = self
            .open_tree(RECORD_NS)
            .context("Opening DB namespace `records` failed")?;
        let uid = cityhasher::hash::<u32>(record_id).to_ne_bytes();
        let valid_through = Utc::now()
            .checked_add_signed(Duration::seconds(retention_period))
            .ok_or(anyhow!("Invalid retention time"))?
            .timestamp();
        records
            .insert(
                uid,
                serde_json::to_vec(&CachedValue {
                    record: record.clone(),
                    valid_through,
                })?,
            )
            .map(|_| ())
            .map_err(Into::into)
    }

    /// Gets NEL-LO context information for a record
    pub fn get_nello_ctx(&self, record_id: &str) -> Result<Option<Vec<NelloContext>>> {
        let nello = self
            .open_tree(NELLO_NS)
            .context("Opening DB namespace `nello` failed")?;
        let uid = cityhasher::hash::<u32>(record_id).to_ne_bytes();
        nello
            .get(uid)
            .map(|m| {
                m.and_then(|cv| {
                    let nello_ctx: Option<Vec<NelloContext>> = serde_json::from_slice(&cv).ok();
                    nello_ctx
                })
            })
            .map_err(Into::into)
    }

    /// Stores a NEL-LO context for a record
    pub fn set_nello_ctx(&mut self, record_id: &str, record: &Vec<NelloContext>) -> Result<()> {
        let nello = self
            .open_tree(NELLO_NS)
            .context("Opening DB namespace `nello` failed")?;
        let uid = cityhasher::hash::<u32>(record_id).to_ne_bytes();
        nello
            .insert(uid, serde_json::to_vec(record)?)
            .map(|_| ())
            .map_err(Into::into)
    }
}

/// Returns an [`IVec`] as a [`u32`] representation
fn ivec_as_u32(ivec: &IVec) -> Result<u32> {
    let four_bytes: [u8; 4] = ivec.as_ref().try_into()?;
    Ok(u32::from_ne_bytes(four_bytes))
}
