FROM scratch
ENTRYPOINT ["/app/app"]
WORKDIR /app
COPY configs/mappings/nello_context.jsonl.gz .
COPY target/app .
