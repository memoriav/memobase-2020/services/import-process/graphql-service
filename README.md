# Memobase GraphQL Service

This service acts as an intermediary between the [Connectome](https://opendatanavigator.switch.ch/) GraphQL API and the [Normalization Service](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/normalization-service). It makes requests for single documents to the GraphQL API on demand, transforms the response to an internal format and caches it for further use.

## Configuration

Almost all settings can be defined in a TOML configuration file, via command-line arguments or environment variables. Command-line arguments take precedence. The configuration file path defaults to `configs/main.toml`, which can be overridden by setting `--config-file <CONFIG_FILE_PATH>`.

| Config file | CLI | Envvar | Description | Required | Default | 
| ----------- | --- | ------ | ----------- | -------- | ------- |
| `host` | `-H` / `--host` | `HOST` | host address | yes | - |
| `serverCertPath` | `--server-cert-path` | `SERVER_CERT_PATH` | path to server certificate file | yes | - |
| `serverKeyPath` | `--server-key-path` | `SERVER_KEY_PATH` | path to server key file | yes | - |
| `caCertPath` | `--ca-cert-path` | `CA_CERT_PATH` | path to CA certificate file; if missing, clients are not verified | no | - |
| `certReloadInterval` | `--cert-reload-interval` | `CERT_RELOAD_INTERVAL` | interval in seconds for reloading certificates | no | `1800` (30 mins) |
| `allowedClientDomains` | `--allowed-client-domains` | `ALLOWED_CLIENT_DOMAINS` | list of allowed client domains | yes | - |
| `graphqlEndpoint` | `-g` / `--graphql-endpoint` | `GRAPHQL_ENDPOINT` | graphQL endpoint URI | yes | - |
| `tokenEndpoint` | `-t` / `--token-endpoint` | `TOKEN_ENDPOINT` | access token endpoint URI | yes | - |
| `tokenRefreshInterval` | `--token-refresh-interval` | `TOKEN_REFRESH_INTERVAL` | access token refresh interval in seconds | no | `7200` |
| `clientId` | `--client-id` | `CLIENT_ID` | client id, required for getting access token | yes | - |
| `clientSecret` | `client-secret` | `CLIENT_SECRET` | client secret, required for getting access token | yes | - |
| `retentionTime` | `--retention-time` | `RETENTION_TIME` | retention time (in seconds) of records in cache | no | `604800` (7 days) | 
| `purgeInterval` | `--purge-interval` | `PURGE_INTERVAL` | interval in seconds at which the cache is emptied from expired documents | no | `3600` (1 hour) |
| `nelloCtxFilePath` | `--nello-ctx-file-path` | `NELLO_CTX_FILE_PATH` | path to NEL-LO context file (see below) | yes | - |
| - | - | `RUST_LOG` | log level | no | `debug` |


### NEL-LO context file

This context file contains additional context information on each record enriched with NEL-LO. As this information can't be requested via the GraphQL API, it has to be manually added. The current workflow entails requesting this file from Switch, converting it with the following command and add it to this repository. You can find the current file in [`configs/mappings/nello_context.jsonl.gz`](./configs/mappings/nello_context.jsonl.gz).

Command to convert context file:

```sh
cat <input_file.json> | jq -cM '.[] | {(.id) : .entities | map(select(.kbid != "NIL") | {text, start, end, kbid})}' | gzip --best > nello_context.jsonl.gz
```

## API for Normalization Service

- `/record/<record_id>`: Request record with identifier `<record_id>`
- `/health`: Health check endpoint

## Further Resources

[Code documentation](https://memoriav.pages.switch.ch/memobase-2020/services/import-process/graphql-service/graphql_service)
